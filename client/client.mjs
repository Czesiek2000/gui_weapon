import * as alt from 'alt';

let webview;
alt.onServer('load', () => {
    if (!webview) {
        webview = new alt.WebView('http://resource/client/html/index.html');
        webview.on('close:Webview', () => {
            alt.showCursor(false);
            webview.destroy();
            webview = undefined;
            
            // Enable controls, it works only when you make it interval function
            // alt.setInterval(() => {
            //     native.enableControlAction(2, 37, true);
            //     native.enableControlAction(2, 50, true);
            //     native.enableControlAction(0, 31, true);
            //     native.enableControlAction(0, 268, true);
            //     native.enableControlAction(0, 33, true);
            //     native.enableControlAction(0, 1, true);
            //     native.enableControlAction(0, 2, true);
            //     native.enableControlAction(0, 3, true);
            //     native.enableControlAction(0, 5, true);
            //     native.enableControlAction(0, 24, true);
            // }, 0);
        });
        
        webview.focus();
        alt.showCursor(true);

        // Disable controls for better usage of CEF
        // alt.setInterval(() => {
        //     native.disableControlAction(2, 37, true); // tab
        //     native.disableControlAction(2, 50, true); // scroll down
        //     native.disableControlAction(0, 31, true); // w
        //     native.disableControlAction(0, 268, true); // s
        //     native.disableControlAction(0, 34, true); // a
        //     native.disableControlAction(0, 1, true); // Lright
        //     native.disableControlAction(0, 2, true); // Ldown
        //     native.disableControlAction(0, 5, true); // Lleft
        //     native.disableControlAction(0, 3, true); // Lup
        //     native.disableControlAction(0, 24, true); // mouse left
        // }, 0);
    }
          
        webview.focus();
        alt.showCursor(true);

        webview.on('spawnWeapon', (name) => {
            alt.emitServer('spawnWeapon', name);
        });

        webview.on('all', () => {
            alt.emitServer('all');
        });

        webview.on('remove', () => {
            alt.emitServer('remove');
        });
});
