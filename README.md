# What is this ?

With this resource you can open and easy give your character all weapons that are available in game.

This resource is made for AltV

## Instalation

* Download this resource from gitlab repository
* Place everything except `README.md` and `weaponHash.json` files in your server folder in the `resource` folder
* Add in your `server.cfg file` inside resources array `gui_weapon` state 
* Everything is ready to use, open your server and AltV client and enjoy new easiest way to add player weapons


# How to use it ?
In the server.mjs file which is located:  server/server.mjs, you can change the command to open gui but by default command is `weaponGui` that opens gui with weapons

# Screenshots from game
Here are some screenshots from the game how it looks
![command](docs/chatCmd.png)

Chat command to open weapon gui

![choice](docs/choice.png)

Navigation to submenu with weapon images 

![example](docs/example.png)

Menu to give player weapon with only one click on the image

## Weapon hash list

I've made json file called `weaponHash.json` which stores all weapon hash that are available in AltV

## Contribute, like
If you like this resource leave a star on gitlab and on the altV forum

## Licence 
This resource is under MIT licence. Feel free to use it on your servers.