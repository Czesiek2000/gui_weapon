import * as alt from 'alt';
import chat from 'chat';

// Open weapon gui menu
chat.registerCmd('weaponGui',(player) => {
    alt.emitClient(player, 'load');
});

// Give player weapon when user click a button
alt.onClient('spawnWeapon', (player, name) => {
    player.giveWeapon(name, 9999, true);
});

// Remove all weapons from player
alt.onClient('remove', (player) => {
    player.removeAllWeapons();
});

// Give player all weapons
alt.onClient('all', (player) => {
    player.giveWeapon('-1834847097', 999, true);
    player.giveWeapon('-1786099057', 999, true);
    player.giveWeapon('-102323637', 999, true);
    player.giveWeapon('-2067956739', 999, true);
    player.giveWeapon('-1569615261', 999, true);
    player.giveWeapon('-1951375401', 999, true);
    player.giveWeapon('1141786504', 999, true);
    player.giveWeapon('1317494643', 999, true);
    player.giveWeapon('-102973651', 999, true);
    player.giveWeapon('-656458692', 999, true);
    player.giveWeapon('-1716189206', 999, true);
    player.giveWeapon('-581044007', 999, true);
    player.giveWeapon('-538741184', 999, true);
    player.giveWeapon('1737195953', 999, true);
    player.giveWeapon('419712736', 999, true);
    player.giveWeapon('-853065399', 999, true);
    player.giveWeapon('-1810795771', 999, true);
    player.giveWeapon('453432689', 999, true);
    player.giveWeapon('3219281620', 999, true);
    player.giveWeapon('1593441988', 999, true);
    player.giveWeapon('584646201', 999, true);
    player.giveWeapon('911657153', 999, true);
    player.giveWeapon('-1716589765', 999, true);
    player.giveWeapon('-1076751822', 999, true);
    player.giveWeapon('-771403250', 999, true);
    player.giveWeapon('137902532', 999, true);
    player.giveWeapon('1198879012', 999, true);
    player.giveWeapon('-598887786', 999, true);
    player.giveWeapon('-1045183535', 999, true);
    player.giveWeapon('324215364', 999, true);
    player.giveWeapon('736523883', 999, true);
    player.giveWeapon('2024373456', 999, true);
    player.giveWeapon('-270015777', 999, true);
    player.giveWeapon('171789620', 999, true);
    player.giveWeapon('-619010992', 999, true);
    player.giveWeapon('-1121678507', 999, true);
    player.giveWeapon('487013001', 999, true);
    player.giveWeapon('2017895192', 999, true);
    player.giveWeapon('-494615257', 999, true);
    player.giveWeapon('-1654528753', 999, true);
    player.giveWeapon('-1466123874', 999, true);
    player.giveWeapon('984333226', 999, true);
    player.giveWeapon('-275439685', 999, true);
    player.giveWeapon('317205821', 999, true);
    player.giveWeapon('-1074790547', 999, true);
    player.giveWeapon('961495388', 999, true);
    player.giveWeapon('-2084633992', 999, true);
    player.giveWeapon('4208062921', 999, true);
    player.giveWeapon('-1357824103', 999, true);
    player.giveWeapon('-1063057011', 999, true);
    player.giveWeapon('2132975508', 999, true);
    player.giveWeapon('1649403952', 999, true);
    player.giveWeapon('-1660422300', 999, true);
    player.giveWeapon('2144741730', 999, true);
    player.giveWeapon('3686625920', 999, true);
    player.giveWeapon('1627465347', 999, true);
    player.giveWeapon('100416529', 999, true);
    player.giveWeapon('205991906', 999, true);
    player.giveWeapon('177293209', 999, true);
    player.giveWeapon('-952879014', 999, true);
    player.giveWeapon('-1312131151', 999, true);
    player.giveWeapon('-1568386805', 999, true);
    player.giveWeapon('1305664598', 999, true);
    player.giveWeapon('1119849093', 999, true);
    player.giveWeapon('2138347493', 999, true);
    player.giveWeapon('1834241177', 999, true);
    player.giveWeapon('1672152130', 999, true);
    player.giveWeapon('125959754', 999, true);
    player.giveWeapon('-1813897027', 999, true);
    player.giveWeapon('-1600701090', 999, true);
    player.giveWeapon('615608432', 999, true);
    player.giveWeapon('741814745', 999, true);
    player.giveWeapon('-1420407917', 999, true);
    player.giveWeapon('126349499', 999, true);
    player.giveWeapon('-1169823560', 999, true);
    player.giveWeapon('600439132', 999, true);
    player.giveWeapon('-37975472', 999, true);
    player.giveWeapon('1233104067', 999, true);
    player.giveWeapon('883325847', 999, true);
    player.giveWeapon('-72657034', 999, true);
    player.giveWeapon('101631238', 999, true);
    player.giveWeapon('2548703416', 999, true);
    player.giveWeapon('2285322324', 999, true);
    player.giveWeapon('2939590305', 999, true);
    player.giveWeapon('1198256469', 999, true);
    player.giveWeapon('1432025498', 999, true);
    player.giveWeapon('2526821735', 999, true);
    player.giveWeapon('2228681469', 999, true);
    player.giveWeapon('1785463520', 999, true);
    player.giveWeapon('3056410471', 999, true);
});

