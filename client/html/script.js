// Get all the imgs to variable
const melee = $('img');

// Loop through all image in array and add event listener when user click each image 
melee.on('click', function () {
    console.log($(this).attr("data-name"));
    // Get attribute which store hash value of the weapon
    let name = $(this).attr("data-name");
    
    // Share parameter with function that send weapon hash value to client and then to server
    spawnWeapon(name);
})

function spawnWeapon(name) {
    alt.emit('spawnWeapon', name);
    console.log(name);
}

$('#cross').on('click', () => {
    // Fire function that destroy webView inside game
    alt.emit('close:Webview');
    $('.container').hide();
})

$('#bMelle').on('click', () => {
    $('#melee').show();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#backM').on('click', () => {
    $('#melee').hide();
    $('#choice').show();
});

$('#backH').on('click', () => {
    $('#hanguns').hide();
    $('#choice').show();
});

$('#back').on('click', () => {
    $('#smg').hide();
    $('#choice').show();
});

$('#backSt').on('click', () => {
    $('#shotguns').hide();
    $('#choice').show();
});

$('#backA').on('click', () => {
    $('#ariffels').hide();
    $('#choice').show();
});

$('#backL').on('click', () => {
    $('#lmg').hide();
    $('#choice').show();
});

$('#backHv').on('click', () => {
    $('#heavy').hide();
    $('#choice').show();
});

$('#backT').on('click', () => {
    $('#throw').hide();
    $('#choice').show();
});

$('#backSn').on('click', () => {
    $('#snipers').hide();
    $('#choice').show();
    console.log('back');
});

$('#backMisc').on('click', () => {
    $('#misc').hide();
    $('#choice').show();
    
});

$('#bHandgun').on('click', () => {
    $('#hanguns').show();
    $('#melee').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bSmg').on('click', () => {
    $('#smg').show();
    $('#melee').hide();
    $('#hanguns').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bShotgun').on('click', () => {
    $('#shotguns').show();
    $('#melee').hide();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bArriffle').on('click', () => {
    $('#ariffels').show();
    $('#melee').hide();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bLmg').on('click', () => {
    $('#lmg').show();
    $('#melee').hide();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bSniper').on('click', () => {
    $('#snipers').show();
    $('#melee').hide();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bHeavy').on('click', () => {
    $('#heavy').show();
    $('#melee').hide();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#throw').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bThrow').on('click', () => {
    $('#throw').show();
    $('#melee').hide();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#misc').hide();
    $('#choice').hide();
});

$('#bMisc').on('click', () => {
    $('#melee').hide();
    $('#hanguns').hide();
    $('#smg').hide();
    $('#shotguns').hide();
    $('#ariffels').hide();
    $('#lmg').hide();
    $('#snipers').hide();
    $('#heavy').hide();
    $('#throw').hide();
    $('#misc').show();
    $('#choice').hide();
});

$('#all').on('click', () => {
    console.log('all');
    alt.emit('all');
});

$('#remove').on('click', () => {
    console.log('remove');
    alt.emit('remove');
});

